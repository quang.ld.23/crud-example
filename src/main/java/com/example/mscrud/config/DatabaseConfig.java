package com.example.mscrud.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configurable
public class DatabaseConfig {

    @Bean
    public DataSource getDataSource(){
        DataSourceBuilder ds = DataSourceBuilder.create();
//        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.driverClassName("org.h2.Driver");
        ds.url("jdbc:mysql://localhost:3306/productmanager");
        ds.username("root");
        ds.password("123456a@");

        return ds.build();
    }
}
