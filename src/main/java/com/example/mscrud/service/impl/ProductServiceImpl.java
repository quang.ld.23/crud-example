package com.example.mscrud.service.impl;

import com.example.mscrud.model.Product;
import com.example.mscrud.repository.ProductRepository;
import com.example.mscrud.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepository _productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this._productRepository = productRepository;
    }

    @Override
    public List<Product> findAllProduct() {
        return (List<Product>) _productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Integer id) {
        return _productRepository.findById(id);
    }

    @Override
    public void save(Product product) {
        _productRepository.save(product);
    }

    @Override
    public void remove(Product product) {
        _productRepository.delete(product);
    }
}